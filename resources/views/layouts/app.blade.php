<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">

    <?php
    $a = [
        array(
            'title' => 'da',
            'url' => 'net'
        ),
        array(
            'title' => 'da 2',
            'url' => 'net 2'
        ),
    ];
    ?>
    @include('layouts.header')

    <main class="py-4">
        <example-component></example-component>
        <prop-component :urldata="{{json_encode($a)}}"></prop-component>
        <ajax-component></ajax-component>
        <chartline-component></chartline-component>
        <chartpie-component></chartpie-component>
        <chartrandom-component></chartrandom-component>
        <socket-component></socket-component>
        <socket-chat-component></socket-chat-component>
        @if (Auth::check())
            пользователь {{Auth::user()->email}}
            <socket-private-component :users="{{ \App\User::select('email', 'id')-> where('id', '!=', Auth::id())->get()}}"
            :user="{{Auth::user()}}"></socket-private-component>
        @endif

        @yield('content')
    </main>
</div>
</body>
</html>
<script>
//    import ChartlineComponent from "../../js/components/ChartlineComponent";
//    export default {
//        components: {ChartlineComponent}
//    }
</script>