@foreach($categories as $category)
    @if ($category->children->where('published', 1)->count())
        <li class="nav-item dropdown">
            <a data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle" href="{{url("/blog/category/$category->slug")}}">{{$category->title}} <span class="caret"></span></a>
            <ul role="menu" class="dropdown-menu">
                @include('layouts.top_menu', [
                'categories' => $category->children])
            </ul>
        </li>
  @else
        <li>
            <a class="nav-link" href="{{url("/blog/category/$category->slug")}}">{{$category->title}}</a>
        </li>
  @endif
@endforeach



