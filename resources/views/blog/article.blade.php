@extends('layouts.app')

@section('title', $article->title . ' - шаблон новости')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>{{ $article->title}}</h1>
                <p>{!! $article->description !!}</p>
            </div>
        </div>



        Категории:
        @forelse($categories as $cat)
             <a href="/blog/category/{{$cat->slug}}">{{$cat->title}}</a>
        @empty
            не заданы
        @endforelse
    </div>
@endsection