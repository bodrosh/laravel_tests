@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('title') Создание Пользователя @endslot
            @slot('parent') Главная @endslot
            @slot('aktive') Пользователи @endslot
        @endcomponent

        <hr>

        <form action="{{route('admin.user_managment.user.store')}}" method="post">
            {{ csrf_field() }}
            @include('admin.user_managment.users.partials.form')
        </form>

    </div>
@endsection
