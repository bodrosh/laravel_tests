
@extends('admin.layouts.app_admin')

@section('content')

    <div class="content">
        @component('admin.components.breadcrumb')
            @slot('title') Список пользователей @endslot
            @slot('parent') Главная @endslot
            @slot('aktive') Новости @endslot
        @endcomponent
    </div>

    <a href="{{route('admin.user_managment.user.create')}}" class="btn btn-primary pull-right"> Создать пользователя</a>

    <table>
        <thead>
        <th>Имя</th>
        <th>email</th>
        <th>Изменить</th>
        <th>Удалить</th>
        </thead>
        @foreach ($users as $user)
            <tr>
                <td>
                    {{$user->name}}
                </td>
                <td>
                    {{$user->email}}
                </td>
                <td>
                    |  <a href="{{route('admin.user_managment.user.edit', $user)}}"> Изменить</a>
                </td>

                <td>
                    <form action="{{route('admin.user_managment.user.destroy', $user)}}" method="post" onsubmit="if(confirm('Удалить?')) { return true } else { return false }" >
                        {{ csrf_field() }}
                        {{--<input type="hidden" name="_method" value="delete" >--}}
                        {{method_field('DELETE')}}
                        <button type="submit" class="btn">Удалить</button>
                    </form>
                </td>
            </tr>
        @endforeach

        <tr>
            <td colspan="2">
                <ul class="pagination pull-right">
                    {{$users->links()}}
                </ul>

            </td>
        </tr>

    </table>

@endsection