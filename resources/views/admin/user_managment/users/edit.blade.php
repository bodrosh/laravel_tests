@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('title') Редактирование Пользователя @endslot
            @slot('parent') Главная @endslot
            @slot('aktive') Пользователи @endslot
        @endcomponent

        <hr>

        <form action="{{route('admin.user_managment.user.update', $user)}}" method="post">
            {{ csrf_field() }}
            @include('admin.user_managment.users.partials.form')
            {{method_field('PUT')}}
            {{--<input type="hidden" name="_method" value="put" >--}}
        </form>

    </div>
@endsection
