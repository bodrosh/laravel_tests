@if($errors->any())
    <div class="allert allert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

<label for="">Имя</label>
<input type="text" class="form-control" name="name" placeholder="Имя" value="@if(old('name')){{old('name')}}@elseif(isset($user -> name)){{$user->name}}@endif" required>

<label for="">Емайл</label>
<input type="text" class="form-control" name="email" placeholder="Емайл" value="@if(old('email')){{old('email')}}@elseif(isset($user -> email)){{$user->email}}@endif" required>

<label for="">Пароль</label>
<input type="password" class="form-control" name="password" placeholder="Пароль" value="">

<label for="">Подтверждение пароля</label>
<input type="password" class="form-control" name="password_confirmation" placeholder="Подтверждение пароля" value="">

<hr />

<input class="btn btn-primary" type="submit" value="Сохранить">