@extends('admin.layouts.app_admin')



@section('content')
    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('title') Изменение категории @endslot
            @slot('parent') Главная @endslot
            @slot('aktive') Категории @endslot
        @endcomponent

        <hr>

        <form action="{{route('admin.category.update', $category)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}
            @include('admin.categories.partials.form')
        </form>


    </div>
@endsection
