@foreach ($categories as $category_list)

    <option value="@if(!empty( $category_list->id )) {{ $category_list->id }} @endif"

            @isset($category->id)

            @if ($category->parent_id == $category_list->id)
            selected=""
            @endif

            @if ($category->id == $category_list->id)
            hidden=""
            @endif

            @endisset

    >
        {!! $delimiter !!}  @if(!empty( $category_list->title )) {{ $category_list->title }} @endif
    </option>

    {{-- проверяем, есть ли у данного пункта вложенные категории--}}

    @if (count($category_list->children) > 0)
        @include('admin.categories.partials.categories', [
          'categories' => $category_list->children,
          'delimiter'  => ' - ' . $delimiter
        ])

    @endif
@endforeach