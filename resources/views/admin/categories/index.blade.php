
@extends('admin.layouts.app_admin')

@section('content')

    <div class="content">
        @component('admin.components.breadcrumb')
            @slot('title') Список категорий @endslot
            @slot('parent') Главная @endslot
            @slot('aktive') Категории @endslot
        @endcomponent
    </div>

    <a href="{{route('admin.category.create')}}" class="btn btn-primary pull-right"> Создать категорию</a>

<table>
    @foreach ($categories as $category)
        <tr>
           <td>
               {{$category->title}}
           </td>
            <td>
                |  <a href="{{route('admin.category.edit', $category)}}"> Изменить</a>
            </td>

            <td>
                <form action="{{route('admin.category.destroy', $category)}}" method="post" onsubmit="if(confirm('Удалить?')) { return true } else { return false }" >
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="delete" >
                    <button type="submit" class="btn">Удалить</button>
                </form>
            </td>
        </tr>
    @endforeach

    <tr>
        <td colspan="2">
            {{$categories->links()}}
        </td>
    </tr>

</table>

@endsection