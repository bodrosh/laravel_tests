
@extends('admin.layouts.app_admin')

@section('content')

    <div class="content">
        @component('admin.components.breadcrumb')
            @slot('title') Список новостей @endslot
            @slot('parent') Главная @endslot
            @slot('aktive') Новости @endslot
        @endcomponent
    </div>

    <a href="{{route('admin.article.create')}}" class="btn btn-primary pull-right"> Создать новость</a>

    <table>
        @foreach ($articles as $article)
            <tr>
                <td>
                    {{$article->title}}
                </td>
                <td>
                    |  <a href="{{route('admin.article.edit', $article)}}"> Изменить</a>
                </td>

                <td>
                    <form action="{{route('admin.article.destroy', $article)}}" method="post" onsubmit="if(confirm('Удалить?')) { return true } else { return false }" >
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete" >
                        <button type="submit" class="btn">Удалить</button>
                    </form>
                </td>
            </tr>
        @endforeach

        <tr>
            <td colspan="2">
                <ul class="pagination pull-right">
                    {{$articles->links()}}
                </ul>

            </td>
        </tr>

    </table>

@endsection