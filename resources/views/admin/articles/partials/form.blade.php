<label for="">Статус</label>
<select class="form-control" name="published">
    @if (isset($article->id))
        <option value="0" @if ($article->published == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($article->published == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>

<label for="title">Заголовок</label>
<input type="text" class="form-control" name="title" placeholder="Заголовок новости" value="@if(isset($article -> title )){{$article -> title}}@endif" required>

<label for="slug">Slug</label>
<input type="text" class="form-control" name="slug" placeholder="Автоматическая генерация" value="@if(isset($article -> slug )){{$article -> slug}}@endif" readonly="">

{{--<input class="form-control" type="text" name="slug" placeholder="Автоматическая генерация" value="{{$category->slug or ""}}" readonly="">--}}

<label for="">Родительская категория</label>
<select class="form-control" name="categories[]" multiple="">
    @include('admin.articles.partials.categories', ['categories' => $categories])
</select>

<label for="">Кратое описание</label>
<textarea class="form-control" name="description_short" id="description-short" placeholder="Автоматическая генерация" >
    @isset($article -> description_short ){{$article -> description_short}} @endisset
</textarea>

<label for="">Полное описание</label>
<textarea class="form-control" name="description" id="description" placeholder="Автоматическая генерация" >
    @isset($article -> description){{$article -> description}} @endisset
</textarea>


<label for="title">meta Заголовок</label>
<input type="text" class="form-control" name="meta_title" placeholder="Заголовок новости" value="@if(isset($article -> meta_title )){{$article -> meta_title}}@endif">


<hr />

<input class="btn btn-primary" type="submit" value="Сохранить">