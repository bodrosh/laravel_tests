@foreach ($categories as $category)

    <option value="@if(!empty( $category->id )) {{ $category->id }} @endif"

            @isset($article->id)
                @foreach ($article->categories as $category_article )
                  @if ($category->id == $category_article->id)
                    selected="selected"
                  @endif
                 @endforeach
            @endisset

    >
        {!! $delimiter !!}  @if(!empty( $category->title )) {{ $category->title }} @endif
    </option>

    {{-- проверяем, есть ли у данного пункта вложенные категории--}}

    @if (count($category->children) > 0)
        @include('admin.articles.partials.categories', [
          'categories' => $category->children,
          'delimiter'  => ' - ' . $delimiter
        ])

    @endif
@endforeach