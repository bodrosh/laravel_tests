@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('title') Редактирование Новости @endslot
            @slot('parent') Главная @endslot
            @slot('aktive') Ноости @endslot
        @endcomponent

        <hr>

        <form action="{{route('admin.article.update', $article)}}" method="post">
            {{ csrf_field() }}
            @include('admin.articles.partials.form')
            <input type="hidden" name="_method" value="put" >
            <input type="hidden" name="modified_by" value="{{Auth::id()}}">
        </form>

    </div>
@endsection
