<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    // данные для аякса
    public function getJson() {
        return [
            array(
                'title' => 'da',
                'url' => 'net'
            ),
            array(
                'title' => 'da 2',
                'url' => 'net 2'
            ),
        ];
    }

    //данные для построения графика
    public function dataChart() {
        return [
            'labels' => ['март', 'апрель', 'май', 'июнь'],
            'datasets' => array([
                'label' => 'Продажи',
               // 'backgroundColor' => '#ec3', или массив цветов, чтобы пирог был разным
                'backgroundColor' => ['#ec3', '#4c3', '#8ca', '#6cb' ],
                'data' => [11500, 1800, 5000, 2000],
            ])
        ];
    }

    //данные для построения графика - случайые данные
    public function chartRandom() {
        return [
            'labels' => ['март', 'апрель', 'май', 'июнь'],
            'datasets' => array(
                [
                    'label' => 'Золото',
                  'backgroundColor' => '#ec3', //или массив цветов, чтобы пирог был разным
                    //'backgroundColor' => ['#ec3', '#4c3', '#8ca', '#6cb' ],
                    'data' => [rand(0,40000), rand(0,40000), rand(0,40000), rand(0,40000)],
                ],
                [
                    'label' => 'Серебро',
                    'backgroundColor' => '#4c3', //или массив цветов, чтобы пирог был разным
                    //'backgroundColor' => ['#ec3', '#4c3', '#8ca', '#6cb' ],
                    'data' => [rand(0,40000), rand(0,40000), rand(0,40000), rand(0,40000)],
                ]
            )
        ];
    }

    // для redis, метод принимает данные из полей формы
    public function newEvent(\Illuminate\Http\Request $request) {
        $result = [
            'labels' => ['март', 'апрель', 'май', 'июнь'],
            'datasets' => array([
                'label' => 'Продажи',
                // 'backgroundColor' => '#ec3', или массив цветов, чтобы пирог был разным
                'backgroundColor' => ['#ec3', '#4c3', '#8ca', '#6cb' ],
                'data' => [11500, 1800, 5000, 2000],
            ])
        ];

        if($request->has('label')) {
            $result['labels'][] = $request->input('label');
            $result['datasets'][0]['data'][] = (integer)$request->input('sale');

            // если чекбок изформы - истина, вызываем событие
            if($request->has('realtime')) {
                if($request->input('realtime') == 'true') {
                    // для передачи данных всем пользователям сначала отправляем их в редис сервер
                    event(new \App\Events\NewEvent($result));
                }
            }
        }

        return $result;
    }


    // отправка сообщений в реатайм
    public function newMessage(\Illuminate\Http\Request $request) {

        event(new \App\Events\NewMessage($request->input('message')));

    }

    // отправка сообщений в приватном чате
    public function newPrivateMessage(\Illuminate\Http\Request $request) {

        //event(new \App\Events\NewMessage($request->input('message')));
        // вызывать события можно по-другому

        \App\Events\PrivateMessage::dispatch($request->all());
        return $request->all();
    }



}
