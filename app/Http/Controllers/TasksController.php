<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App;

class TasksController extends Controller
{
    /*
     *  все задачи
     */
    public function index() {
        $tasks = App\Task::all(); // все его значения
        return view('tasks.index', compact('tasks'));
    }

    /*
     * выводим конкретную задачу
     */
    public function show( $id ) {
        $task = App\Task::find( $id );
        return view('tasks.show', compact('task'));
    }
}
