<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    // либо заполняемые поля, либо запрещенные к заполнению (пустой массив, нет запрещенных)
    protected $guarded = [];

    // многие ко многим с gjkmpjdfntkzvb
    public function users() {
        return $this->belongsToMany('App\User');
    }
}
