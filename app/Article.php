<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str; // для генерации слага


class Article extends Model
{
    protected $fillable = ['title', 'slug', 'description_short', 'description', 'image', 'image_show', 'meta_title', 'published', 'viewed', 'created_by', 'modified_by'];

    // делаем слаг
    public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug( mb_substr($this->title, 0, 40) . "-" . \Carbon\Carbon::now()->format('dmyHi'), '-');
    }

    // Полиморфная связь с категориями
    public function categories() {
        return $this->morphToMany('App\Category', 'categoryable'); //categoryable - приставка от categoryable_id
    }

    public function scopeLastArticles($query, $count) {
        return $query->orderBy('created_by', 'desc')->take($count)->get();
    }
}
