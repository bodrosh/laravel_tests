<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str; // для генерации слага

class Category extends Model
{

  // сделаем разрешенный списо изменения полей
    protected $fillable = ['title', 'slug', 'parent_id', 'published', 'created_by', 'modified_by'];

    // делаем слаг
    public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug( mb_substr($this->title, 0, 40) . "-" . \Carbon\Carbon::now()->format('dmyHi'), '-');
    }

    //получаем дочерние категории
    public function children() {
        //
       return $this->hasMany(self::class, 'parent_id');
      //  return $this->hasMany('App\Category', 'parent_id');
    }

    // Полиморфная связь с новостями
    public function articles() {
        return $this->morphedByMany('App\Article', 'categoryable'); //categoryable - приставка от categoryable_id
    }

    public function scopeLastCategories($query, $count) {
        return $query->orderBy('created_by', 'desc')->take($count)->get();
    }
}
