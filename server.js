var http = require('http').Server(); // подключаем модуль http, запускаем сервер, наподобии апача
var io = require('socket.io')(http); // реализует связь с клиентом в браузере (протокол websocket)
var Redis = require('ioredis'); // чтобы иметь доступ к серверу, используем библиотеку

// новый экземпляр redis
var redis = new Redis();

//для получения значений с сервера redis нам нужно подписаться на канал (кот. указан в app/Events/NewEvent.php )
//redis.subscribe('news-action');

//для отслеживания множества каналов, а не ондого, меняем метод выше на
redis.psubscribe('news-action.*');
// * зведочка - маска, т.е. изменяемая часть
//и меняем функцию ниже

// и запустить прослушивание (слежение)
//redis.on('message', function(channel, message) {
redis.on('pmessage', function(pattern, channel, message) {
    console.log('Message recieved: ' +  message); // выведем в консоль сервера
    console.log('Channel: ' +  channel);
    message = JSON.parse(message); // содержимое приходит в jsone, распарсим

    // отправим данные всем клиентам, передаем название канала и пр-во имен событий
    io.emit(channel + ':' + message.event, message.data);
});

// запустим сервер
http.listen(3000, function() {
    console.log('Listening on Port: 3000');
})