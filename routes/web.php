<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('blog.home');
});


Route::get('/chat', function () {
    return view('chat.index');
});

// плохая запись - не писать код в файле маршрутов
Route::post('/messages', function (Illuminate\Http\Request $request) {
  //  App\Events\Message::dispatch($request->input('body'));
   // App\Events\PrivateChat::dispatch($request->input('body'));
    App\Events\PrivateChat::dispatch($request->all());
});

/*
 * Выводим все задачи
 */
//Route::get('/tasks', function () {
//    $tasks = DB::table('tasks')->get();
//    //dd( $tasks );
//    return view('tasks.index', compact('tasks'));
//});
// То же самое через контроллер модели
//Route::get('/tasks', function () {
//    $tasks = App\Task::all(); // все его значения
//    return view('tasks.index', compact('tasks'));
//});

/// Ну а лучше через специальный контроллер
Route::get('/tasks', 'TasksController@index');


/*
 * Выводим выполненные задачи
 */
//Route::get('/incomplete-tasks', function () {
//    $tasks = App\Task::incomplete(); // все его значения
//    return view('tasks.index', compact('tasks'));
//});

/*
 * Выводим одну задачу подробно
 */
//Route::get('/tasks/{task}', function ( $id ) {
//    $task = DB::table('tasks')->find( $id );
////   dd( $task );  //- как print_r();
//    return view('tasks.show', compact('task'));
//});

//Route::get('/tasks/{task}', function ( $id ) {
//    $task = App\Task::find( $id );
//    return view('tasks.show', compact('task'));
//});

/// Ну а лучше через специальный контроллер
Route::get('/tasks/{task}', 'TasksController@show');



Route::get('/posts', 'PostsController@index');

//Route::get('/send-test-mail', 'MailController@send');


Auth::routes();
Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['auth']], function(){
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    Route::resource('/category', 'CategoryController', ['as' => 'admin']);
    Route::resource('/article', 'ArticleController', ['as' => 'admin']);
    Route::group(['prefix'=>'user_managment', 'namespace'=>'UserManagment'], function() {
        Route::resource('/user', 'UserController', ['as' => 'admin.user_managment']);
    });
});


// slug? может быть, а может и не быть
Route::get('/blog/category/{slug?}', 'BlogController@category')->name('category');
Route::get('/blog/article/{slug?}', 'BlogController@article')->name('article');

Route::get('/start/get-json', 'TestController@getJson');
Route::get('/start/data-chart', 'TestController@dataChart');
Route::get('/start/random-chart', 'TestController@chartRandom');
Route::get('/start/socket-chart', 'TestController@newEvent');
Route::get('/start/send-message', 'TestController@newMessage');
Route::get('/start/send-private-message', 'TestController@newPrivateMessage');


// комнаты
Route::get('/room/{room}', function(App\Room $room) {
    return view('chat.room',
        ['room' => $room]
    );
});